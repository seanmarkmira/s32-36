//json web token
	// sign - function that creates a token
	// verify - function that checks if there's presence of token
	// decode - function that decodes the token
const jwt = require('jsonwebtoken')
const secret = "MagandaAko"

module.exports.createAccessToken = (user) => {

	const data = {
		id:user._id,
		email:user.email,
		isAdmin: user.isAdmin
	}

	//jwt.sign(payload, secret, {options})
	return jwt.sign(data, secret, {})
}


module.exports.decode = (token) => {
	//jwt.decode(token, {})

	let slicedToken = token.slice(7, token.length)
	//console.log(jwt.decode(slicedToken, {complete:true}))
	return jwt.decode(slicedToken, {complete:true}).payload
	//return jwt.decode(slicedToken)
}

module.exports.verify = (req,res,next) => {
	let token = req.headers.authorization

	if(!token){
		res.send({auth: "failed/Token Problem", message:"There is no req.headers.authorization"})
	}else{
		
	let slicedToken = token.slice(7, token.length)

	if(typeof slicedToken !== "undefined"){
		return jwt.verify(slicedToken, secret, (err,data)=>{
			if(err){
				res.send({auth: "failed/Token Problem"})
			}else{
				next()
			}
		})
	}else{
		res.send(false)
	}
	}

}

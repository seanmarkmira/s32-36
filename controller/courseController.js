const Course = require('../models/Course')
const auth = require('../auth.js')

//createToken is just used for creating token and testing purposes
const createToken = async(req,res)=>{
	console.log(req.body)
	res.status(200).json({access: auth.createAccessToken(req.body)})
	//return {access: auth.createAccessToken(result)}
}

const createCourse = async (req,res)=>{
	try{
		const createCourse = await Course.create(req.body)
		res.status(200).json({msg:'create course completed', status:true, result: createCourse})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
	
}
const getAllCourses = async (req,res) => {
	const allCourses = await Course.find({})
	res.status(200).json(allCourses)
}

const archive = async(req,res) =>{
	console.log('in archive')
	const id = req.params.userId

	try{
		const updateCourse = await Course.findByIdAndUpdate(id, {isActive: false}, {new:true})
		res.status(200).json({msg:'Course has been updated', status:true, result: updateCourse})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
	
}

const unArchive = async(req,res) =>{
	console.log('in unarchive')
	const id = req.params.userId

	try{

		const updateCourse = await Course.findByIdAndUpdate(id, {isActive: true}, {new:true})
		res.status(200).json({msg:'Course has been updated', status:true, result: updateCourse})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
	

}

const getActiveCourses = async(req,res) => {
	console.log('you are in active courses')
	try{

		const result = await Course.find({isActive:"true"})
		
		console.log(result)
		res.status(200).json({msg:'These are the active courses', status:"success", result: result})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
}

const getACourses = async(req,res)=>{
	const id = req.params.userId

	try{
		const result = await Course.find({id})
		res.status(200).json({msg:'This is the specific document', status:'success', result: result})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
}

const editCourse = async(req,res)=>{
	console.log(req.body)
	console.log('inside edit course')
	const id = req.body._id

	const updateBody = {
		"courseName": req.body.courseName,
		"description": req.body.description,
		"price": req.body.price,
		"isActive": req.body.isActive
	}

	try{
		const updateCourse = await Course.findByIdAndUpdate(id, updateBody, {new:true})
		res.status(200).json({msg:'This is the specific document', status:'success', result: updateCourse})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
}

const deleteCourse = async(req, res)=>{
	id = req.params.userId

	try{
		const deleteResult = await Course.findByIdAndDelete(id)
		console.log(deleteResult)
		res.status(200).json({msg:'Deleted a document', status:'success', result: deleteResult})
	}catch(error){
		res.status(500).json({msg:error, status:false})
	}
}

module.exports = {
	createToken,
	deleteCourse,
	editCourse,
	getACourses,
	getActiveCourses,
	archive,
	unArchive,
	createCourse,
	getAllCourses
}

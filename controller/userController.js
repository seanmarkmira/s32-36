const User = require('../models/User')
const auth = require('../auth.js')
const bcrypt = require('bcrypt')
const Course = require('../models/Course')

const checkEmail =()=>{
	User.findOne({email: email}).then( (result, error) => {
		if(result != null){
			return (false)
		} else {
			if(result == null){
				return (true)
			} else {
				return (error)
			}
		}
	})
}

const  getAllUsers = async (req,res)=>{
	const result = await User.find({})
	res.status(200).json({result})
}

const register = (reqBody)=>{
	console.log('we are in register')
	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email: email,
		password: bcrypt.hashSync(password, 10),
		mobileNo: mobileNo,
		age: age
	})
	return newUser.save().then((result,error) =>{
		console.log(result)
	})
}

const login = (reqBody) => {
	const {email, password} = reqBody

	return User.findOne({email:email}).then((result,error)=>{
		console.log(result)

		if(result == null){
			console.log('email null')
			return false
		}else{

			let isPwCorrect = bcrypt.compareSync(password, result.password)

			if(isPwCorrect == true){
				//return json web token
			//invoke the function which creates the token upon logging in
			//requirements in creating a token:
				// if password matches from existing pw from db
				return {access: auth.createAccessToken(result)}
				//can also be
				//return auth.createAccessToken(result)
			}else{
				return false
			}
		}
	})
}

const editProfile = (userId, reqBody) =>{
	// const result =  User.findByIdAndUpdate({_id:userId})
	console.log(userId)
	console.log(reqBody)
	return User.findByIdAndUpdate(userId, reqBody,{returnDocument:'after'}).then(result=>{
		console.log(result)
	})
}

const getUserProfile = (reqBody)=>{
	return User.findById({_id: reqBody.id}).then(result => {
		result.password = 'encrypted through strings'
		console.log(result.password)
		return result;
	})
}

const editUser = (userId, reqBody) =>{
	const {firstName, lastName, email, password, mobileNo, age} = reqBody
	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}

	return User.findByIdAndUpdate(userId, updatedUser, {new:true}).then(result=>{
		result.password = "*****"
		console.log(result)
	})
}

const editDetails = async(req,res)=>{
	const result = User.findOne({email:req.body.email})
	const finalUpdate = await User.findOneAndUpdate(result,req.body,{new:true})
	finalUpdate.password = "****"
	res.status(200).json(finalUpdate)
}

const findIDAndDelete = async(req,res)=>{
	const deleteDoc = await User.findByIdAndDelete({_id:req.params.userId})
	if(!deleteDoc){
		res.status(500).json({msg:'no document found'})
	}else{
		res.status(200).json({msg:true})
	}
}

const deleteUser = async(req,res)=>{
	console.log(req.body.email)
	const deleteResult = await User.findOneAndDelete({email: req.body.email})
	if(!deleteResult){
		res.status(500).json({msg:'no document found'})
	}else{
		res.status(200).json({msg:true})
	}
}
// There are still bugs in enroll function
const enroll = async(data)=>{
	const {userId, courseId} = data;
	console.log(data)

	const userEnroll = await User.findById(userId).then((result, err)=>{
		if(err){
			return err
		}else{
			result.enrollments.push({
				courseId: courseId
			})
			return result.save().then(result=>true)
		}
	})

	const courseEnroll = await Course.findById(courseId).then((result,err)=>{
		if(err){
			return err
		}else{
			result.enrollees.push({
				userId:userId
			})
			return result.save().then(result =>{
				return true
			})
		}
	})

	if(userEnroll && courseEnroll){
		return true
	}else{
		return false
	}
}

//deleteUser function with utilizing the token
/*
 const email = auth.decode(req.header.authorization).email
 const deleteResult = await User.findOneAndDelete({email: email})
*/


module.exports = {
	enroll,
	deleteUser,
	findIDAndDelete,
	editDetails,
	editUser,
	editProfile,
	checkEmail,
	getUserProfile,
	register,
	getAllUsers,
	login
}
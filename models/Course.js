const mongoose = require('mongoose');

//Create a Schema
const courseSchema = new mongoose.Schema(
	{
		courseName:{
			type: String,
			require: [true, "Course name is required"]
		},
		description: {
			type: String,
			require: [true, "Course description is required"]
		},
		price: {
			type: Number,
			require: [true, "Course price is required"]
		},
		isActive:{
			type: Boolean,
			default: true
		},
		createdOn:{
			type: Date,
			default: new Date()
		},
		enrollees:[
			{
				userId: {
					type: String,
					required: [true, "Course name is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
);

module.exports = mongoose.model("Course", courseSchema);
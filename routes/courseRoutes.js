const {getAllCourses, createCourse, archive, unArchive, getActiveCourses, getACourses, editCourse, deleteCourse, createToken} = require('../controller/courseController')
const express = require('express');
const router = express.Router();
const auth = require('./../auth')

//app.use("/api/courses", courseRoutes)
//createToken is just used for creating token and testing purposes
router.route('/createtoken').post(createToken)
router.route('/').get(getAllCourses)
router.route('/create-course').post(auth.verify, createCourse)
router.route('/:userId/archive').put(auth.verify, archive)
router.route('/:userId/unarchive').put(auth.verify, unArchive)
router.route('/active-courses').get(auth.verify, getActiveCourses)
router.route('/:userId/specific-course').get(auth.verify, getACourses)
router.route('/edit-course').put(auth.verify, editCourse)
router.route('/:userId/delete').delete(auth.verify, deleteCourse)

module.exports = router
const userController = require('../controller/userController')
const express = require('express');
const router = express.Router();
const auth = require('./../auth')

// router.get('/', (req, res) => {
// 	res.status(200).send('You have accessed the home page')
// })

//enrollments
router.post("/enroll", auth.verify,(req,res)=>{
	let data = {
		userId:auth.decode(req.headers.authorization).id,
		courseId:req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
})

router.post('/email-exists', (req,res)=>{
	userController.checkEmail(req.body.email).then(result=>res.send(result))
})

router.post('/register',(req,res)=>{
	userController.register(req.body)
})

router.post('/login', (req,res)=>{
	userController.login(req.body).then(result=>res.send(result))
})

router.get('/details', auth.verify, (req,res)=>{
	let userData = auth.decode(req.headers.authorization)
	userController.getUserProfile(userData).then(result=> res.send(result))
})

router.get('/edit', auth.verify, (req,res)=>{
	// console.log(req.headers.authorization)
	// console.log(auth.decode(req.headers.authorization).id)
	let payload = auth.decode(req.headers.authorization)
	userController.editUser(payload.id)
})

router.put('/:userId/edit', (req,res)=>{
	let userId = auth.decode(req.headers.authorization).id
	// const userId = req.params.userId
	userController.editProfile(userId, req.body).then(result=>res.send(result))
})

router.route('/delete').delete(userController.deleteUser)
router.route('/:userId/delete').delete(userController.findIDAndDelete)
router.route('/edit-profile').put(userController.editDetails)
router.route('/').get(userController.getAllUsers)

module.exports = router;